﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.Entity;
using System.IO;

namespace DataGopi.Models
{
    public class AppSeedData
    {
        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SeedData//";

            var context = serviceProvider.GetService<AppDbContext>();
            context.Database.Migrate();

            context.Malls.RemoveRange(context.Malls);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);
            SeedMallsFromCsv(relPath, context);

            /*var l1 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA", NoOfShoppingMalls = 3, MallNames = "x" };
            var l2 = new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA", NoOfShoppingMalls = 40, MallNames =  "y" };
            var l3 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA", NoOfShoppingMalls = 3, MallNames =  "z"  };
            var l4 = new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA", NoOfShoppingMalls = 42, MallNames =  "a"};
            var l5 = new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India", NoOfShoppingMalls = 53, MallNames = "1" };
            var l6 = new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India", NoOfShoppingMalls = 23, MallNames = "y" };
            var l7 = new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Dream Land", State = "Minnesota", Country = "USA", NoOfShoppingMalls = 62, MallNames = "4"};

            //           new Mall() { Name = "X", IsEntertainable = true, IsPlayStore = false, IsTheater = true }
            context.Locations.AddRange(l1, l2, l3, l4, l5, l6, l7);

            context.SaveChanges();

            context.Malls.AddRange(
                new Mall() { Name = "X", IsEntertainable = true, IsPlayStore = false, IsTheater = true, LocationID = l1.LocationID },
                new Mall() { Name = "Y", IsEntertainable = false, IsPlayStore = false, IsTheater = true , LocationID = l2.LocationID },
                new Mall() { Name = "Z", IsEntertainable = true, IsPlayStore = false, IsTheater = false, LocationID = l3.LocationID },
                new Mall() { Name = "A", IsEntertainable = true, IsPlayStore = true, IsTheater = true, LocationID = l4.LocationID },
                new Mall() { Name = "B", IsEntertainable = false, IsPlayStore = false, IsTheater = true , LocationID = l5.LocationID },
                new Mall() { Name = "C", IsEntertainable = true, IsPlayStore = true, IsTheater = true , LocationID = l6.LocationID },
                new Mall() { Name = "D", IsEntertainable = false, IsPlayStore = false, IsTheater = true, LocationID = l7.LocationID }
                );
            context.SaveChanges();*/
        }

        

        
        private static void SeedMallsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "mall.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            Mall.ReadAllFromCSV(source);
            List<Mall> lst = Mall.ReadAllFromCSV(source);
            context.Malls.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedLocationsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }

    }
}
