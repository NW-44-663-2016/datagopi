using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using DataGopi.Models;

namespace DataGopi.Migrations
{
    [DbContext(typeof(AppDbContext))]
    partial class AppDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataGopi.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("MallNames");

                    b.Property<int>("NoOfShoppingMalls");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("DataGopi.Models.Mall", b =>
                {
                    b.Property<int>("MallId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsEntertainable");

                    b.Property<bool>("IsPlayStore");

                    b.Property<bool>("IsTheater");

                    b.Property<int>("LocationID");

                    b.Property<string>("Name");

                    b.HasKey("MallId");
                });

            modelBuilder.Entity("DataGopi.Models.Mall", b =>
                {
                    b.HasOne("DataGopi.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });
        }
    }
}
