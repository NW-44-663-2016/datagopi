using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataGopi.Models;

namespace DataGopi.Controllers
{
    public class MallsController : Controller
    {
        private AppDbContext _context;

        public MallsController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: Malls
        public IActionResult Index()
        {
            var appDbContext = _context.Malls.Include(m => m.Location);
            return View(appDbContext.ToList());
        }

        // GET: Malls/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Mall mall = _context.Malls.Single(m => m.MallId == id);
            if (mall == null)
            {
                return HttpNotFound();
            }

            return View(mall);
        }

        // GET: Malls/Create
        public IActionResult Create()
        {
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location");
            return View();
        }

        // POST: Malls/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Mall mall)
        {
            if (ModelState.IsValid)
            {
                _context.Malls.Add(mall);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", mall.LocationID);
            return View(mall);
        }

        // GET: Malls/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Mall mall = _context.Malls.Single(m => m.MallId == id);
            if (mall == null)
            {
                return HttpNotFound();
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", mall.LocationID);
            return View(mall);
        }

        // POST: Malls/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Mall mall)
        {
            if (ModelState.IsValid)
            {
                _context.Update(mall);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", mall.LocationID);
            return View(mall);
        }

        // GET: Malls/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Mall mall = _context.Malls.Single(m => m.MallId == id);
            if (mall == null)
            {
                return HttpNotFound();
            }

            return View(mall);
        }

        // POST: Malls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Mall mall = _context.Malls.Single(m => m.MallId == id);
            _context.Malls.Remove(mall);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
